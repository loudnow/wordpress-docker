# Get started

This documentation will walk you through the LoudNow WordPress Environment setup. It's encapsulated, dev focused toolchain that covers for you:

- Basic WordPress setup with must-use plugins included
- Database seed that includes examples of desired workflow
- Layed down project structure for component driven architecture
- Build tools to bundle your assets and sync the browser on changes
- Dotfiles for static code linting and hints
 
 We'll keep it short. Lets go!

## Prerequisites

Make sure you have already installed both [Docker Engine](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/). You'll need [node]() as well with gulp-cli installed globally `npm install gulp-cli -g`.

## Installation

Run from your terminal

`npm install`

Then make sure your MAMP / WAMP / XAMPP or other local apache apps are disabled to avoid conflicts and run:

`docker-compose up --detach`

After that the site is exposed on `localhost:80` and for your convinience we bundled phpmyadmin at `localhost:8080`

You can login to preinstalled Wordpress at:
```
localhost:80/wp-admin
user: wordpress
pass: wordpress
```

## Development 

We included basic asstes bundle with browser livereload. You just run the below command and you're good to code:

`npm run dev`

Other available commands are:

`npm start` - runs all underlying tasks to build the project
`npm run lint` - css and js linters

Talking 'bout linters. We provided all settings via root dotfiles. You'd be better if you install `stylelint` and `eslint` editor plugins, it will give you instant feedback loop and help you to adjust to company standards. If you'd ignore linters your code will be blocked from commit with precommit hooks.

## Git

We set up git workflow per project basis. You'll create feature branches out of `develop` - this should contain code that is merged after code review.

## Architecture & Styleguide

### General

Commmon settings and reusable pieces of css and js live in `theme/lib` folder. They are not included anywhere by default, you can pick what you need at any time and not worry about output bloat. Entry points for each of `twig`, `js`, `scss` are located in `theme/views/templates/**` as we're bundling our assets per template. Import all global seetings here and pick sections that you use on particular template.

The `theme/views` structure by role:

1. Elements - the most basic ui parts (button, input, avatar). They mostly can live without twig file to preserve simplicity.
2. Components - The biggest group probably, you will reuse `Elements` here and combine them into header, footer card, and other ui patterns
3. Sections - should map 1:1 with ACF sections - this is logical baseline for communication with the product owner, he will always mindmap application structure to what is given to him in admin panel and he shouldn't worry about underlying architecture.
4. Templates - should map 1:1 to routing, this is where we pick which sections are used on particular view and import all the assets as well as pass the data

### ACF

We build out application made out of editorial sections (just like feature-chart one). The DB is seeded with an example for that. You'll notice custom ACF plugin called `acf-component-field`. It's used to encapsulate definition of the section into component that can be reused. You will often need to place the same section on a different page and this is a great way to keep things DRY and have single point to maintain (the changes propagate to other instances). So basically - always create new Field Group and make it as a component (below pubish button), then use the component on another Field Group.

The ACF component should map 1:1 with `views/sections`

The Field Groups that include components should map 1:1 with `views/templates`

It creates transparent way of how data clicks with frontend code and application business logic.

### Twig

[timber.github.io/docs/](https://timber.github.io/docs/)

`Twig` templates are used via `Timber` plugin. We try to use it extensively. It gets you nice OOP way of interacting with WordPress data, Routing (we use it to map our templates to pages), Image object that generates images on fly and can help with generating lazyloaded responsive (srcset) images, that's great time saving.

You will notice within our examples that we use `embeds` that have `block` that serves as a slot for content that might differ between use cases. You can pass props to it the same way you do with `include` using `with` parameter

### Scss

[sass-lang.com/](https://sass-lang.com/)

We write styles with `scss`. Some basic settings are provided in `theme/lib` to give you an idea on file organisation. There are also some useful mixins included for `font-face` generation and file `exports` (shield against duplication).

Use `export` mixin across all your files and import them whenever you're reusing those styles in sibling `index.twig`. The idea is to safely build bigger and bigger components without worrying about code duplication and dependency tree. You'll always pull components that are one level lower in hierarchy but are parents to multiple subcomponents - you don't need to bother about them at any time

#### BEM

[en.bem.info/methodology/](https://en.bem.info/methodology/)

Naming convention for classes is written with BEM methodology. We try to avoid unnessesary specificity that is often produced by missuse of Scss nesting. BEM is a great cure, however if you'll push it to hard and attach Block to something that is in the middle of the component chain you might end up tight coupling components by scoping it to parent block

```scss
.section {
  ...

  &__heading {
    // if you're all styles for heading live here you probably do it wrong. 
    // Keep here just positioning of the element in relation to it's section. 
    // The face styles that describe its look should go to separate .heading class for better reusability.
  }
}
```

More on that here [en.bem.info/methodology/block-modification/#using-a-mix-to-change-a-block](https://en.bem.info/methodology/block-modification/#using-a-mix-to-change-a-block)

#### Nesting

Nesting in general makes it harder to tell at a glance where css selector optimizations can be made. Avoid it unless it's being used as a convenience to extend the parent selector over targeting nested elements. For example:

Good:
```scss
.block {
  ...

  &--mini {
    ...
  }

  &.is-active {} // allowed for state classes

  .has-block-active & {} // when you want to pass parent state
}
```

Bad:
```scss
.block {
  ...

  &.other-block {
    ...
  }
}
```