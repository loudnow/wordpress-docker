export default (el, events, fn) => {
  events.split(' ').forEach(e => el.addEventListener(e, fn, false))
}
