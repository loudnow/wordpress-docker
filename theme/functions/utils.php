<?php
/**
 * Common helper functions and others that doesn't belong to any special grouping.
 * Feel free to refactor when patters emerge.
 */

function ln_site_url () {
  $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
  return $protocol . $_SERVER[ 'HTTP_HOST' ] . $_SERVER[ 'REQUEST_URI' ];
}

function ln_load_template ( $name ) {
  return function () use ($name) {
    $context = Timber::get_context();
    $context[ 'post' ] = new Timber\Post( url_to_postid( LN_SITE_URL ) );
  
    wp_enqueue_style( 'lnn-styles', get_stylesheet_directory_uri() . '/dist/' . $name . '/styles.css');
    wp_enqueue_script( 'lnn-scripts', get_stylesheet_directory_uri() . '/dist/' . $name . '/scripts.js', [], false, true ); // load in footer
  
    Timber::render( 'views/templates/' . $name . '/index.twig', $context );
  };
}
