<?php

function add_live_reload () {
  if (WP_DEBUG) {
    echo "<script>document.write('<script src=\"http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1\"></' + 'script>')</script>";
  }
}

add_filter( 'wp_footer', 'add_live_reload');
