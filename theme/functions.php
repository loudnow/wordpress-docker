<?php
/**
 * Split functions file into smaller units and require it here
 */

require_once( 'functions/utils.php' );
require_once( 'functions/defines.php' );
require_once( 'functions/live-reload.php' );

/**
 * Define all routes here and connect them with top level twig templates
 * located in 'view/templates'. The js and css assets are registered and enqueued 
 * per route from 'dist' folder where are built bundels are stored.
 */
Routes::map( 'pricing', ln_load_template( 'pricing' ) );
// Example for nested route. Try to keep templates folder flat
// Routes::map( 'pricing/:name', ln_load_template( 'pricing-single' ) );
