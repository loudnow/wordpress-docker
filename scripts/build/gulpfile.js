// Load plugins
const autoprefixer = require('autoprefixer')
// const browsersync = require('browser-sync').create()
const cssnano = require('cssnano')
const del = require('del')
const eslint = require('gulp-eslint')
const gulp = require('gulp')
const data = require('gulp-data')
const imagemin = require('gulp-imagemin')
const livereload = require('gulp-livereload')
const newer = require('gulp-newer')
const plumber = require('gulp-plumber')
const postcss = require('gulp-postcss')
const rename = require('gulp-rename')
const reporter = require('postcss-reporter')
const sass = require('gulp-sass')
const stylelint = require('stylelint')
const syntaxScss = require('postcss-scss')
const uglify = require('gulp-uglify')
const webpack = require('webpack')
const webpackstream = require('webpack-stream')
const webpackconfig = require('./webpack.js')

// const url = 'loudnow.local'

// BrowserSync
// function browserSync (done) {
//   browsersync.init({
//     notify: false,
//     scrollElementMapping: ['.page'],
//     proxy: url,
//   })
//   done()
// }

// // BrowserSync Reload
// function livereloadReload (done) {
//   browsersync.reload()
//   done()
// }

// Clean assets
function clean () {
  return del(['../../theme/dist'], {
    dot: true,
    force: true,
  })
}

// Optimize Images
function images () {
  return gulp
    .src('../../theme/images/**/*')
    .pipe(newer('../../theme/dist/images'))
    .pipe(
      imagemin({
        progressive: true,
      })
    )
    .pipe(gulp.dest('../../theme/dist/images'))
}

// Lint styles
function stylesLint () {
  const processors = [
    stylelint(),
    reporter({
      clearMessages: true,
      throwError: true,
    }),
  ]

  return gulp
    .src('../../theme/**/*.scss')
    .pipe(postcss(processors, {
      syntax: syntaxScss,
    }))
}

// Lint scripts
function scriptsLint () {
  return gulp
    .src(['../../theme/**/*.js', './scripts/**/*.js'])
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
}

// CSS task
function css () {
  const browserList = [
    'ie >= 11',
    'ie_mob >= 11',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10',
  ]

  return gulp
    .src('../../theme/views/templates/**/*.scss')
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'expanded',
    }))
    .pipe(gulp.dest('../../theme/dist/'))
    .pipe(rename({
      suffix: '.min',
    }))
    .pipe(postcss([autoprefixer(browserList), cssnano()]))
    .pipe(gulp.dest('../../theme/dist/'))
    .pipe(livereload())
    // .pipe(browsersync.stream())
}

// Transpile, concatenate and minify scripts
function scripts () {
  let path, name

  // That's a bit funky but webpack stream returns it's own file info to the pipe
  // We need to preserve original info and reassign it for desired outcome
  const saveFileinfo = file => {
    const relativePathArr = file.path.split('/templates/')[1].split('/')
    path = '../../theme/dist/'
    path += relativePathArr.slice(0, -1).join('/')
    name = relativePathArr.slice(-1)[0].replace('.js', '')
  }

  return gulp
    .src(['../../theme/views/templates/**/*.js'])
    .pipe(plumber())
    .pipe(data(saveFileinfo))
    .pipe(webpackstream(webpackconfig), webpack)
    .pipe(rename(path => { path.basename = name }))
    .pipe(gulp.dest(() => path))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min',
    }))
    .pipe(gulp.dest(() => path))
    .pipe(livereload())
    // .pipe(browsersync.stream())
}

// Watch files
function watchFiles () {
  livereload.listen()
  gulp.watch('../../theme/**/*.scss', gulp.series(stylesLint, css))
  gulp.watch('../../theme/**/*.js', gulp.series(scriptsLint, scripts))
  gulp.watch('../../theme/images/**/*', gulp.series(images))
  // gulp.watch('../../theme/**/*.php', gulp.series())
}

// Tasks
gulp.task('images', images)
gulp.task('css', gulp.series(stylesLint, css))
gulp.task('js', gulp.series(scriptsLint, scripts))
gulp.task('clean', clean)

// lint
gulp.task('lint', gulp.parallel(stylesLint, scriptsLint))

// build
gulp.task('build', gulp.series(clean, gulp.parallel(images, 'css', 'js')))

// watch
gulp.task('watch', gulp.parallel(watchFiles))

// dev
gulp.task('dev', gulp.series('build', 'watch'))
